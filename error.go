package tools

import (
	"errors"
	"net/http"
)

var (
	ErrBadRequest          = NewErrorResponse(http.StatusBadRequest, errors.New("Bad Request"))
	ErrUnauthorized        = NewErrorResponse(http.StatusUnauthorized, errors.New("Unauthorized"))
	ErrNotFound            = NewErrorResponse(http.StatusNotFound, errors.New("Not Found"))
	ErrConflict            = NewErrorResponse(http.StatusConflict, errors.New("Conflict"))
	ErrInternalServerError = NewErrorResponse(http.StatusInternalServerError, errors.New("Internal Server Error"))
)

type ErrorResponse struct {
	StatusCode int    `json:"-"`
	Key        string `json:"key,omitempty"`
	Message    string `json:"message,omitempty"`
}

func NewErrorResponse(code int, err error) *ErrorResponse {
	return &ErrorResponse{
		StatusCode: code,
		Message:    err.Error(),
	}
}

func (e *ErrorResponse) Error() string {
	return e.Message
}

func (e *ErrorResponse) SetKey(key string) {
	e.Key = key
	return
}

func ErrorToResponse(err error) (errResp *ErrorResponse) {
	switch e := err.(type) {
	case *ErrorResponse:
		errResp = e
	default:
		errResp = NewErrorResponse(http.StatusInternalServerError, err)
	}
	return errResp
}
