package tools

import (
	"encoding/json"
	"errors"
	"fmt"
	"html/template"
	"log"
	"os"
	"os/signal"
	"reflect"
	"runtime"
	"strconv"
	"strings"
)

var logger = log.Println
var printer = fmt.Println

type Reanimator func(v ...interface{})

var ReanimateImitator Reanimator = func(v ...interface{}) {}

func Reanimate(f Reanimator, v ...interface{}) {
	if r := recover(); r != nil {
		v = append(v, r)
		f(v...)
	}
}

func DontPanicAndGetError(i interface{}) (err error) {
	err = errors.New("unknown panic")
	switch x := i.(type) {
	case []interface{}:
		if len(x) > 0 {
			err = DontPanicAndGetError(x[0])
		}
	case string:
		err = errors.New(x)
	case error:
		err = x
	}
	return
}

func TryGetVariableOrDefault(tryVariable, defaultVariable interface{}) (variable interface{}, err error) {
	variable = defaultVariable
	defer Reanimate(func(i ...interface{}) {
		err = DontPanicAndGetError(i)
	})
	iv := reflect.ValueOf(tryVariable)
	dv := reflect.ValueOf(defaultVariable)
	rv := reflect.New(dv.Type())
	rv.Elem().Set(iv)
	variable = rv.Elem().Interface()
	return
}

//TryAndDontPanicAgain

func CringeCatcher(f func()) (err error) {
	defer Reanimate(func(r ...interface{}) {
		err = DontPanicAndGetError(r)
	})
	f()
	return
}

func FindInSlice(slice []interface{}, val interface{}) (int, bool) {
	for i, item := range slice {
		if item == val {
			return i, true
		}
	}
	return -1, false
}

// TODO: test
func WaitForSignal() {
	keys := make(chan os.Signal, 1)
	signal.Notify(keys, os.Interrupt)
	s := <-keys
	log.Printf("Got signal: %v, exiting.", s)
}

// TODO: test
func ParseTypeToString(val interface{}) (str string) {
	switch val.(type) {
	case int, uint:
		str = fmt.Sprintf("%d", val)
	case float32, float64:
		str = fmt.Sprintf("%f", val)
	case string:
		str = fmt.Sprintf("%s", val)
	default:
		str = fmt.Sprintf("%v", val)
	}
	return
}

// TODO: test
func MarshalUnmarshal(from, to interface{}) (err error) {
	data, err := json.Marshal(from)
	if hasError(err) {
		return
	}
	err = json.Unmarshal(data, &to)
	return
}

// TODO: test
func ParseTemplate(text string, data interface{}) (string, error) {
	temp := template.Must(template.New("email").Parse(text))
	builder := &strings.Builder{}
	err := temp.Execute(builder, data)
	return builder.String(), err
}

type Mapper map[string]reflect.Value

// TODO: test
func (mapper Mapper) Fill(i interface{}) {
	v := reflect.ValueOf(i)
	for i := 0; i < v.NumField(); i++ {
		if alias, ok := v.Type().Field(i).Tag.Lookup("json"); ok {
			if alias != "" {
				switch v.Field(i).Kind() {
				case reflect.Ptr:
					mapper[alias] = v.Field(i).Elem()
				default:
					mapper[alias] = v.Field(i)
				}
			}
		}
	}
	return
}

// TODO: test
func (mapper Mapper) GetStringByKey(key string) (str string) {
	switch mapper[key].Kind() {
	case reflect.Int, reflect.Uint:
		str = fmt.Sprintf("%d", mapper[key].Uint())
	case reflect.Float32, reflect.Float64:
		str = strconv.FormatFloat(mapper[key].Float(), 'f', -1, 32)
	case reflect.String:
		str = mapper[key].String()
	}
	return
}

// TODO: test
func GetFunctionName(i interface{}) string {
	return runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
}

func hasError(err error) bool {
	return err != nil
}
