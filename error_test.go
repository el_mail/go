package tools

import (
	"errors"
	"fmt"
	"net/http"
	"testing"
)

func TestNewErrorResponse(t *testing.T) {
	defaultStatusCode := http.StatusTeapot
	err := errors.New("error message")
	want := ErrorResponse{
		StatusCode: defaultStatusCode,
		Message:    err.Error(),
	}

	errResp := NewErrorResponse(defaultStatusCode, err)
	isNotEqual := errResp.StatusCode != want.StatusCode || errResp.Message != want.Message
	if isNotEqual {
		t.Errorf("NewErrorResponse(%v) = %v; want %v", err, errResp, want)
	}
}

func TestErrorToResponse(t *testing.T) {
	defaultError := errors.New("default error message")
	var defaultErrorResp = &ErrorResponse{
		StatusCode: http.StatusInternalServerError,
		Message:    defaultError.Error(),
	}
	var customErrorResp = &ErrorResponse{
		StatusCode: http.StatusTeapot,
		Message:    "custom error message",
	}
	var testCases = []struct {
		err  error
		want *ErrorResponse
	}{
		{defaultError, defaultErrorResp},
		{customErrorResp, customErrorResp},
	}

	for _, tt := range testCases {
		testname := fmt.Sprintf("(%+v)", tt.err)
		t.Run(testname, func(t *testing.T) {
			errResp := ErrorToResponse(tt.err)
			isNotEqual := errResp.StatusCode != tt.want.StatusCode || errResp.Message != tt.want.Message
			if isNotEqual {
				t.Errorf("TestErrorToResponse(%+v) = %+v; want %+v", tt.err, errResp, tt.want)
			}
		})
	}
}
