package tools

import (
	"fmt"
	"testing"
)

type DefaultTestSubStruct struct{}

type DefaultTestGroupStruct struct {
	DefaultTestSubStructPtr *DefaultTestSubStruct
}

type DefaultTestStruct struct {
	DefaultTestGroupStructPtr *DefaultTestGroupStruct
}

func TestIdle(t *testing.T) {

}

func TestReanimate(t *testing.T) {
	checkReanimate := func(dts DefaultTestStruct) bool {
		defer Reanimate(ReanimateImitator)
		_ = dts.DefaultTestGroupStructPtr.DefaultTestSubStructPtr
		return true
	}

	defaultTestStruct := DefaultTestStruct{}
	checkIsFaild := checkReanimate(defaultTestStruct)

	if checkIsFaild {
		t.Errorf("TestReanimate() check is faild")
	}
}

func TestDontPanicAndGetError(t *testing.T) {
	checkDontPanicAngGetError := func(dts DefaultTestStruct) (err error) {
		defer Reanimate(func(r ...interface{}) {
			err = DontPanicAndGetError(r)
		})
		_ = dts.DefaultTestGroupStructPtr.DefaultTestSubStructPtr
		return
	}

	defaultTestStruct := DefaultTestStruct{}
	err := checkDontPanicAngGetError(defaultTestStruct)
	noError := !hasError(err)

	if noError {
		t.Errorf("TestDontPanicAndGetError() doesn't catch error")
	}
}

func TestCringeCatcher(t *testing.T) {
	err := CringeCatcher(func() {
		defaultTestStruct := DefaultTestStruct{}
		_ = defaultTestStruct.DefaultTestGroupStructPtr.DefaultTestSubStructPtr
	})
	noError := !hasError(err)

	if noError {
		t.Errorf("TestCringeCatcher() doesn't catch error")
	}
}

func TestTryGetVariableOrDefault(t *testing.T) {
	var testCases = []struct {
		v, dv interface{}
		want  interface{}
	}{
		{true, false, true},
		{2.0, 0.1, 2.0},
		{'r', 'd', 'r'},
		{"string", "defaultString", "string"},
		{nil, "defaultString", "defaultString"},
		{"", 'r', 'r'},
		{struct{}{}, DefaultTestStruct{}, DefaultTestStruct{}},
	}

	for _, tt := range testCases {
		testname := fmt.Sprintf("(%v,%v)", tt.v, tt.dv)
		t.Run(testname, func(t *testing.T) {
			value, err := TryGetVariableOrDefault(tt.v, tt.dv)
			isNotEqual := value != tt.want

			if isNotEqual || (isNotEqual && hasError(err)) {
				t.Errorf("TryGetVariableOrDefault(%v, %v) = %v; want %v.(%T)", tt.v, tt.dv, value, tt.want, tt.want)
			}
		})
	}
}

func TestFindInSlice(t *testing.T) {
	index, ok := FindInSlice([]interface{}{1, "a", 3, 4, 5}, "a")
	want := 1
	isNotEqual := index != want
	isNotOk := !ok

	if isNotEqual && isNotOk {
		t.Errorf(`FindInSlice([]interface{}{1, "a", 3, 4, 5}, "a") = (%v %v); want (%d true)`, index, ok, want)
	}
}
